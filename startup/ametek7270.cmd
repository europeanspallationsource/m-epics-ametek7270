require ametek7270,tomaszbrys
#require ametek7270,waynelewis
require autosave,5.7.0

### channel finder, must be cf server setup...
#require recsync,1.2.0
#dbLoadRecords(reccaster.db, P=P:)

#epicsEnvSet(EPICS_CA_ADDR_LIST,10.0.2.15)
epicsEnvSet(EPICS_CA_MAX_ARRAY_BYTES,1000000)

#Specify the TCP endpoint and give your 'bus' an arbitrary name eg. "ametek7270stream".
drvAsynIPPortConfigure("stream", "10.4.3.150:50000")

# Stop readout at the first null byte. The device sends two extra
# bytes (status and overload) which will be flushed each time. 
asynOctetSetInputEos("stream", 0, "\0")

#asynSetTraceMask(  "stream", -1, 0x9)
#asynSetTraceIOMask("stream", -1, 0x2)
 
#Load your database defining the EPICS records
dbLoadRecords("ametek7270.template", "P=A, Q=M, PROTOCOL=ametekLockIn.proto, PORT=stream")

set_savefile_path(".autosave/sav")
set_requestfile_path(".autosave/req")

#set_pass0_restoreFile("info_positions.sav")
set_pass0_restoreFile("info_settings.sav")
set_pass1_restoreFile("info_settings.sav")

iocInit()

cd .autosave/req
makeAutosaveFiles()
cd ../..
create_monitor_set("info_positions.req", 5, "")
create_monitor_set("info_settings.req", 15, "")
dbl > pvList.txt
